import { Audio, IAudioReadable } from 'audiocommons-es-core';
import { AudioFx, AudioFxChain } from 'audiocommons-es-fx';

import { AudioSample } from './audio-sample';

type TSample = {
		sample: AudioSample;
		chain: AudioFxChain;
		playing: boolean;
}

export class AudioSampler<E extends string = string> extends Audio implements IAudioReadable {
	private sources: Map<E, TSample> = new Map<E, TSample>();

	public addSample(
			id: E,
			sample: AudioSample
	): void {
		this.sources.set(
				id,
				{
						sample: sample,
						chain: new AudioFxChain(sample),
						playing: false
				}
		);
	}

	public removeSample(id: E): void {
		this.sources.delete(id);
	}

	public setLoop(id: E, loop: boolean): void {
		if (!this.sources.has(id)) return;

		this.sources.get(id)!.sample.loop = loop;
	}

	public addFx(id: E, fx: AudioFx): void {
		if (!this.sources.has(id)) return;

		this.sources.get(id)!.chain.addFx(fx);
	}

	public play(id: E): boolean {
		if (!this.sources.has(id)) return false;

		this.sources.get(id)!.playing = true;
		return true;
	}

	public rewind(id: E): void {
		if (!this.sources.has(id)) return;

		this.sources.get(id)!.sample.rewind();
	}

	public replay(id: E): boolean {
		this.rewind(id);
		return this.play(id);
	}

	public stop(id: E): boolean {
		if (!this.sources.has(id)) return false;

		this.sources.get(id)!.playing = false;
		return true;
	}

	public async read(
			dest: number[][],
			offset: number,
			length: number
	): Promise<number> {
		const buffer: number[][] = [];
		const zero: number[] = [];
		for (let c = this.audioFormat.channels; c-- > 0;) zero.push(0);
		for (let l = length, i = 0; l-- > 0; i++) buffer.push([ ...zero ]);

		const batches: number[][][] = [];
		for (const id of this.sources.keys()) {
			const source: TSample = this.sources.get(id)!;
			if (!source.playing) continue;

			const read: number = await source.chain.read(buffer, 0, length);
			batches.push(
					buffer.slice(0, read)
							.map((sample: number[]): number[] => [ ...sample ])
			);
		}

		for (let c = this.audioFormat.channels; c-- > 0;) {
			for (let l = length; l-- > 0;) dest[l][c] = 0;

			for (const batch of batches) {
				for (let l = batch.length, i = 0, o = offset; l-- > 0; o++, i++) {
					dest[o][c] += batch[i][c];
				}
			}
		}

		return length;
	}
}
