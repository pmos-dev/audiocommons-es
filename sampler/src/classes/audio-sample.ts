import { Audio, IAudioFormat, IAudioReadable } from 'audiocommons-es-core';

const LOAD_BUFFER_SIZE: number = 10240;

export class AudioSample extends Audio implements IAudioReadable {
	private internalOffset: number = 0;
	private internalLoop: boolean;

	public static async load(
			source: IAudioReadable
	): Promise<AudioSample> {
		const buffers: number[][][] = [];

		const buffer: number[][] = [];
		const zero: number[] = [];
		for (let c = source.audioFormat.channels; c-- > 0;) zero.push(0);

		for (let i = 0; i < LOAD_BUFFER_SIZE; i++) buffer.push([ ...zero ]);

		while (true) {
			const read: number = await source.read(buffer, 0, LOAD_BUFFER_SIZE);
			if (read === 0) break;

			buffers.push(
					buffer.slice(0, read)
							.map((sample: number[]): number[] => [ ...sample ])
			);
		}

		const samples: number[][] = [];
		for (const b of buffers) {
			samples.push(...b);
		}

		return new AudioSample(source.audioFormat, samples);
	}

	constructor(
			audioFormat: IAudioFormat,
			private samples: number[][],
			loop: boolean = false
	) {
		super(audioFormat);

		this.internalLoop = loop;
	}

	public get loop(): boolean {
		return this.internalLoop;
	}

	public set loop(loop: boolean) {
		this.internalLoop = loop;
	}

	public rewind(): void {
		this.internalOffset = 0;
	}
	
	// eslint-disable-next-line @typescript-eslint/require-await
	public async read(dest: number[][], offset: number, length: number): Promise<number> {
		if (this.samples.length === 0) return 0;

		let read: number = 0;

		if (this.internalOffset >= this.samples.length) return 0;

		for (let i = length, j = offset; i-- > 0; j) {
			dest[j++] = [ ...this.samples[this.internalOffset] ];
			read++;

			this.internalOffset++;
			if (this.internalOffset >= this.samples.length) {
				if (this.internalLoop) {
					this.internalOffset = 0;
				} else {
					break;
				}
			}
		}
		
		return read;
	}
}
