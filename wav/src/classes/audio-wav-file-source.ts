import * as fs from 'fs';

import { AudioFile } from 'audiocommons-es-core';
import { IAudioFormat } from 'audiocommons-es-core';
import { IAudioReadable } from 'audiocommons-es-core';

import { IAudioWavChunk } from '../interfaces/iaudio-wav-chunk';
import { IAudioWavFormatChunk } from '../interfaces/iaudio-wav-format-chunk';

import {
		GROUP_ID,
		FILE_LENGTH,
		RIFF_TYPE,

		CHUNK_SIZE,
		FORMAT_TAG,
		CHANNELS,
		SAMPLES_PER_SEC,
		AVERAGE_BYTES_PER_SEC,
		BLOCK_ALIGN
} from '../consts/audio-wav';

type TFsPromisesRead = {
		bytesRead: number;
		buffer: Buffer;
};

type TResultWithNewPosition<T> = {
		result: T;
		newPosition: number;
};

async function consume(
		file: fs.promises.FileHandle,
		position: number,
		size: number
): Promise<number> {
	const blackhole: Buffer = Buffer.alloc(size);

	const read: TFsPromisesRead = await file.read(blackhole, 0, size, position);

	return read.bytesRead;
}

async function readChunkMetadata(
		file: fs.promises.FileHandle,
		position: number
): Promise<TResultWithNewPosition<IAudioWavChunk>> {
	const data: Buffer = Buffer.alloc(GROUP_ID + CHUNK_SIZE);

	const read: TFsPromisesRead = await file.read(data, 0, data.length, position);
	position += read.bytesRead;
	
	const view: DataView = new DataView(data.buffer);

	const metadata: IAudioWavChunk = {
			groupId: data.toString().substring(0, 4),
			chunkSize: view.getInt32(GROUP_ID, true)
	};
	
	return {
			result: metadata,
			newPosition: position
	};
}

async function readWavFormat(
		file: fs.promises.FileHandle,
		metadata: IAudioWavChunk,
		position: number
): Promise<TResultWithNewPosition<IAudioWavFormatChunk>> {
	const data: Buffer = Buffer.alloc(metadata.chunkSize);

	const read: TFsPromisesRead = await file.read(data, 0, metadata.chunkSize, position);
	position += read.bytesRead;
	
	const view: DataView = new DataView(data.buffer);

	const wavFormat: IAudioWavFormatChunk = {
			groupId: metadata.groupId,
			chunkSize: metadata.chunkSize,
			formatTag: view.getUint16(0, true),
			channels: view.getUint16(FORMAT_TAG, true),
			samplesPerSec: view.getUint32(FORMAT_TAG + CHANNELS, true),
			avgBytesPerSec: view.getUint32(FORMAT_TAG + CHANNELS + SAMPLES_PER_SEC, true),
			blockAlign: view.getUint16(FORMAT_TAG + CHANNELS + SAMPLES_PER_SEC + AVERAGE_BYTES_PER_SEC, true),
			bitsPerSample: view.getUint16(FORMAT_TAG + CHANNELS + SAMPLES_PER_SEC + AVERAGE_BYTES_PER_SEC + BLOCK_ALIGN, true)
	};
	
	return {
			result: wavFormat,
			newPosition: position
	};
}

async function readHeaderAndAudioFormat(
		file: fs.promises.FileHandle
): Promise<TResultWithNewPosition<IAudioFormat>> {
	// get the header out of the way
	let position: number = GROUP_ID + FILE_LENGTH + RIFF_TYPE;
	
	const result: TResultWithNewPosition<IAudioWavChunk> = await readChunkMetadata(file, position);
	position = result.newPosition;

	const metadata: IAudioWavChunk = result.result;
	if (metadata.groupId !== 'fmt ') throw new Error('Unexpected data other than format chunk in WAV header');

	const result2: TResultWithNewPosition<IAudioWavFormatChunk> = await readWavFormat(file, metadata, position);
	position = result2.newPosition;

	const wavFormat: IAudioWavFormatChunk = result2.result;

	const audioFormat: IAudioFormat = {
			channels: wavFormat.channels,
			bitDepth: wavFormat.bitsPerSample,
			sampleRate: wavFormat.samplesPerSec
	};

	return {
			result: audioFormat,
			newPosition: position
	};
}

export class AudioWavFileSource extends AudioFile implements IAudioReadable {
	public static async for(
			file: fs.promises.FileHandle,
			loop: boolean
	): Promise<AudioWavFileSource> {
		const result: TResultWithNewPosition<IAudioFormat> = await readHeaderAndAudioFormat(file);

		const source: AudioWavFileSource = new AudioWavFileSource(
				file,
				result.result,
				result.newPosition,
				loop
		);

		await source.skipNonDataChunks();

		return source;
	}

	public static async forFilename(
			filename: string,
			loop: boolean
	): Promise<AudioWavFileSource> {
		const handle: fs.promises.FileHandle = await fs.promises.open(filename, 'r');
		return await AudioWavFileSource.for(handle, loop);
	}

	protected constructor(
			file: fs.promises.FileHandle,
			audioFormat: IAudioFormat,
			private position: number,
			loop: boolean
	) {
		super(
				file,
				audioFormat,
				loop
		);
	}

	private async consume(size: number): Promise<number> {
		const read: number = await consume(this.file, this.position, size);

		this.position += read;

		return read;
	}

	private async readChunkMetadata(): Promise<IAudioWavChunk> {
		const result: TResultWithNewPosition<IAudioWavChunk> = await readChunkMetadata(this.file, this.position);

		this.position = result.newPosition;

		return result.result;
	}

	// this is only actually used to push the position forward during a reset
	private async skipHeaderAndAudioFormat(): Promise<void> {
		const result: TResultWithNewPosition<IAudioFormat> = await readHeaderAndAudioFormat(this.file);

		this.position = result.newPosition;
	}

	private async skipNonDataChunks(): Promise<void> {
		while (true) {
			const metadata: IAudioWavChunk = await this.readChunkMetadata();
			if (metadata.groupId === 'data') break;
			
			await this.consume(metadata.chunkSize);
		}
	}

	public async reset(): Promise<void> {
		// no need to set position to 0, as the skipHeaderAndAudioFormat already does that

		await this.skipHeaderAndAudioFormat();
		await this.skipNonDataChunks();
	}

	public async read(
			dest: number[][],
			offset: number,
			length: number,
			ttl: number = 10	// only allow loop recurse up to 10 times
	): Promise<number> {
		if (ttl <= 0) return 0;	// to prevent infinite loops if the file is empty or corrupt

		const bytes: number = length * this.bytesPerSample;
		const buffer: Buffer = Buffer.alloc(bytes);

		const read: TFsPromisesRead = await this.file.read(
				buffer,
				0,
				bytes,
				this.position
		);
		this.position += read.bytesRead;

		if (read.bytesRead === 0 && this.loop) {
			await this.reset();
			return await this.read(dest, offset, length, ttl--);
		}

		return this.bufferToSamples(buffer, 0, read.bytesRead, dest, offset);
	}
}
