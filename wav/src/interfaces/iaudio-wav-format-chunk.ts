import { IAudioWavChunk } from './iaudio-wav-chunk';

export interface IAudioWavFormatChunk extends IAudioWavChunk {
		formatTag: number;
		channels: number;
		samplesPerSec: number;
		avgBytesPerSec: number;
		blockAlign: number;
		bitsPerSample: number;
}
