export interface IAudioWavChunk {
		groupId: string;
		chunkSize: number;
}
