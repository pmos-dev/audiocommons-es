import * as fs from 'fs';

import { IAudioFormat } from '../interfaces/iaudio-format';

import { Audio } from './audio';

export abstract class AudioFile extends Audio {
	constructor(
			protected file: fs.promises.FileHandle,
			audioFormat: IAudioFormat,
			protected loop: boolean = false
	) {
		super(audioFormat);
	}

	public async close(): Promise<void> {
		await this.file.close();
	}
}
