import { IAudioReadable } from '../interfaces/iaudio-readable';
import { IAudioFormat } from '../interfaces/iaudio-format';

export class AudioSilentSource implements IAudioReadable {
	constructor(
			private internalAudioFormat: IAudioFormat
	) {}

	public get audioFormat(): IAudioFormat {
		return { ...this.internalAudioFormat };
	}
	
	// eslint-disable-next-line @typescript-eslint/require-await
	public async read(dest: number[][], offset: number, length: number): Promise<number> {
		for (let i = length; i-- > 0;) {
			for (let c = this.internalAudioFormat.channels; c-- > 0;) {
				dest[offset + i][c] = 0;
			}
		}
		
		return length;
	}
}
