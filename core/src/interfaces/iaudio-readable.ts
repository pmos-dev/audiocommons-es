import { IAudioFormat } from './iaudio-format';

export interface IAudioReadable {
		get audioFormat(): IAudioFormat;
		read(dest: number[][], offset: number, length: number): Promise<number>;
}
