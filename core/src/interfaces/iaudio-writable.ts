import { IAudioFormat } from './iaudio-format';

export interface IAudioWritable {
		get audioFormat(): IAudioFormat;
		write(src: number[][], offset: number, length: number): Promise<void>;
}
