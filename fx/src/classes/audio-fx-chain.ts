import { IAudioFormat, IAudioReadable } from 'audiocommons-es-core';

import { AudioFx } from './audio-fx';

export class AudioFxChain implements IAudioReadable {
	private chain: AudioFx[] = [];
	
	constructor(
			private src: IAudioReadable
	) {}

	public get audioFormat(): IAudioFormat {
		return this.src.audioFormat;
	}

	public addFx(fx: AudioFx): void {
		this.chain.push(fx);
	}

	public async read(dest: number[][], offset: number, length: number): Promise<number> {
		const buffer: number[][] = new Array(length)
				.fill(null)
				.map((_: undefined): number[] => [ 0, 0 ]);

		const read: number = await this.src.read(buffer, 0, length);

		for (let i = 0; i < read; i++) {	// this has to go forward as some of the effects rely on forward linear
			for (let c = this.src.audioFormat.channels; c-- > 0;) {
				for (const fx of this.chain) fx.transform(buffer[i]);

				dest[offset + i][c] = buffer[i][c];
			}
		}

		return read;
	}
}
