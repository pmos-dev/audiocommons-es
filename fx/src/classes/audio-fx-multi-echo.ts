import { IAudioFormat } from 'audiocommons-es-core';

import { AudioFx } from './audio-fx';

export class AudioFxMultiEcho extends AudioFx {
	private history: number[][] = [];
	private writeOffset: number;
	private readOffsets: number[] = [];
	private max: number;
	private count: number;
	
	constructor(
		private lengths: number[],
		private depths: number[],
		bpm?: number,
		audioFormat?: IAudioFormat
	) {
		super();

		if (bpm) {
			if (!audioFormat) throw new Error('Length as bpm without audioFormat');
			this.lengths = this.lengths
					.map((length: number): number => AudioFx.bpmSampleSize(bpm, audioFormat) * length);
		}

		if (this.lengths.length !== this.depths.length) throw new Error('Lengths and depths arrays must be the same size');
		this.count = this.lengths.length;

		this.max = Math.max(...this.lengths);
		
		this.history = new Array(this.max).fill([] as number[]) as number[][];
		this.reset();

		this.writeOffset = 0;
	}
	
	private reset(): void {
		this.history = this.history.map((_: unknown): number[] => [ 0, 0 ]);

		this.readOffsets = [];
		for (let i = 0; i < this.count; i++) this.readOffsets.push(this.max - this.lengths[i]);
	}
	
	public set enabled(enabled: boolean) {
		super.enabled = enabled;
		this.reset();
		this.writeOffset = 0;
	}
	
	protected apply(sample: number[]): void {
		const retain: number[] = sample.slice();
		
		for (let i = this.count, j = 0; i-- > 0; j++) {
			const existing: number[] = this.history[this.readOffsets[j]];
			sample[0] += existing[0] * this.depths[j];
			sample[1] += existing[1] * this.depths[j];
			
			this.readOffsets[j]++;
			if (this.readOffsets[j] >= this.max) this.readOffsets[j] = 0;
		}
		
		this.history[this.writeOffset++] = [ retain[0], retain[1] ];
		if (this.writeOffset >= this.max) this.writeOffset = 0;
	}
}
