import { AudioFxEcho } from './audio-fx-echo';

export class AudioFxChorus extends AudioFxEcho {
	constructor() {
		super(250, 0.8);
	}
}
