import { IAudioTransformable } from 'audiocommons-es-core';
import { IAudioFormat } from 'audiocommons-es-core';

export abstract class AudioFx implements IAudioTransformable {
	public static bpmSampleSize(bpm: number, audioFormat: IAudioFormat): number {
		return Math.floor((60 / bpm) * audioFormat.sampleRate);
	}
	
	private internalEnabled: boolean = false;
	
	public set enabled(enabled: boolean) {
		this.internalEnabled = enabled;
	}

	protected abstract apply(sample: number[]): void;
	
	public transform(sample: number[]): void {
		if (!this.internalEnabled) return;

		this.apply(sample);
	}
}
