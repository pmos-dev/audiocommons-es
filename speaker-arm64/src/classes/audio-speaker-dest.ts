import Speaker from 'speaker-arm64';

import { Audio } from 'audiocommons-es-core';
import { IAudioWritable } from 'audiocommons-es-core';
import { IAudioFormat } from 'audiocommons-es-core';

export class AudioSpeakerDest extends Audio implements IAudioWritable {
	private speaker: Speaker;
	
	constructor(
			audioFormat: IAudioFormat
	) {
		super(audioFormat);
		
		this.speaker = new Speaker(audioFormat);
	}
	
	public write(
			src: number[][],
			offset: number,
			length: number
	): Promise<void> {
		const bytes: number = length * this.bytesPerSample;
		const buffer: Buffer = Buffer.alloc(bytes);
		
		this.samplesToBuffer(src, offset, length, buffer, 0);
		
		return new Promise((resolve: () => void, _: (reason: any) => void): void => {
			this.speaker.write(
					buffer,
					(): void => {
						resolve();
					}
			);
		});
	}
}
